package com.example.posts.models;

public class Post implements Comparable<Post> {
    int  userId;
    int id;
    String title;
    String body;

    public Post(int userId, int id, String title, String body) {
        this.userId = userId;
        this.id = id;
        this.title = title;
        this.body = body;
    }

    public int getUserId() {
        return userId;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    @Override
    public int compareTo(Post post) {
        return Integer.compare(this.id,post.getId());
    }
}
