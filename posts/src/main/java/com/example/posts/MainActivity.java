package com.example.posts;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.example.posts.adapters.PostListAdapter;
import com.example.posts.models.Post;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements PostListAdapter.PostListAdapterEvents {
    ListView lv;
    PostListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lv = findViewById(R.id.lv);
        List<Post> posts = new ArrayList<>();
        posts.add(new Post(1, 100, "Первый", "Ничего не понял"));
        posts.add(new Post(1, 200, "Второй", "Ничего не понял"));
        posts.add(new Post(1, 300, "Третий", "Ничего не понял"));
        posts.add(new Post(1, 400, "Четвертый", "Ничего не понял"));
        posts.add(new Post(1, 500, "Пятый", "Ничего не понял"));
        posts.add(new Post(1, 600, "Шестой", "Ничего не понял"));
        posts.add(new Post(1, 700, "Седьмой", "Ничего не понял"));
        posts.add(new Post(1, 800, "Восьмой", "Ничего не понял"));
        posts.add(new Post(1, 900, "Девятый", "Ничего не понял"));
        posts.add(new Post(1, 1000, "Десятый", "Ничего не понял"));
        posts.add(new Post(2, 100, "Одинадцатый", "Ничего не понял"));
        adapter = new PostListAdapter(posts,this);
        lv.setAdapter(adapter);

    }

        @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // TODO Auto-generated method stub

        menu.add("отменить удаление");



        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        adapter.cancel();
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onClickPost(Post post) {
        Toast.makeText(this,String.valueOf(post.getId()),Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClickUserIdName(String userIdName) {
        Toast.makeText(this,userIdName,Toast.LENGTH_LONG).show();
    }
}
