package com.example.posts.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.posts.R;
import com.example.posts.models.Post;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class PostListAdapter extends BaseAdapter {
    private List<Post> posts;
    PostListAdapterEvents listner;
    private List<PostListWrapper> data;
    List<Post> cancelList;

        public void cancel(){
        posts.addAll(cancelList);
        init();
        notifyDataSetChanged();
    }
    public interface PostListAdapterEvents{
        void onClickPost(Post post);
        void onClickUserIdName(String userIdName);
    }
    public class PostListWrapper {
        Post post;
        String userIdName;
        Integer userId;

        public PostListWrapper(String userIdname, Integer userId, Post post) {
            this.post = post;
            this.userIdName = userIdname;
            this.userId = userId;
        }
    }
public void init(){
        data=new ArrayList<>();

    Set<Integer> userIds = new HashSet();
    for (Post post:posts){
        userIds.add(post.getUserId());
    }
    for (Integer id:userIds){

      List<Post> userIdNameList=new ArrayList<>();
        data.add(new PostListWrapper(String.valueOf(id),id,null));
        for(Post post:posts){
            if (post.getUserId()==id){
                userIdNameList.add(post);

            }
        }
        Collections.sort(userIdNameList);

                for (Post post:userIdNameList) {
            data.add(new PostListWrapper(null, null, post));
        }

    }

}
    public PostListAdapter(List<Post> posts,PostListAdapterEvents listner){
        this.posts=posts;
        this.listner=listner;
        init();
    }
private void deletePosts(Integer userId){
    Iterator<Post> iterator=posts.iterator();
    cancelList=new ArrayList<>();
    while(iterator.hasNext()){
        Post post=iterator.next();
        if(post.getUserId()==userId){
            cancelList.add(post);
            iterator.remove();
        }
    }
}
    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public PostListWrapper getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final PostListWrapper wrapper=getItem(position) ;
        if (getItemViewType(position)==0){
            if(convertView==null){
convertView=LayoutInflater.from(parent.getContext()).inflate(R.layout.user_id_listitem,null);
            }
            TextView txt = convertView.findViewById(R.id.userIdName);
            Button btn=convertView.findViewById(R.id.btnDeleteGroup);
            final String userIdName = wrapper.userIdName;

            txt.setText(wrapper.userIdName);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                   deletePosts(wrapper.userId);
                   init();
                   notifyDataSetChanged();
                }
            });
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listner.onClickUserIdName(userIdName);
                }
            });
        }
        else {
            if (convertView == null) {
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_listitem, null);
            }
            TextView txtUserId = convertView.findViewById(R.id.userId);
            TextView txtId = convertView.findViewById(R.id.id);
            TextView txtTitle = convertView.findViewById(R.id.title);
            TextView txtBody = convertView.findViewById(R.id.body);
            Button btnDeletePost=convertView.findViewById(R.id.deletePost);
            final Post post = wrapper.post;

            txtUserId.setText(String.valueOf(post.getUserId()));
            txtId.setText(String.valueOf(post.getId()));
            txtTitle.setText(post.getTitle());
            txtBody.setText(post.getBody());
            btnDeletePost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    posts.remove(post);
                    init();
                    notifyDataSetChanged();
                }
            });
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listner.onClickPost(post);
                }
            });
        }
        return convertView;
    }
    @Override
    public int getItemViewType(int position){
        if(getItem(position).userIdName !=null){
            return 0;
        }
        return 1;
    }
    @Override
    public int getViewTypeCount() {
        return 2;
    }
}
