package com.example.asynctasks;

import android.os.AsyncTask;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements TimerTask.TimerTaskEvents {
    private TimerTask timerTaskSec;
    private TimerTask timerTaskMin;
    private TimerTask timerTaskHour;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Button btnStart = findViewById(R.id.btnStart);
        Button btnPause = findViewById(R.id.btnPause);


        Button btnResume = findViewById(R.id.btnResume);
        btnResume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timerTaskSec.resume();
                timerTaskMin.resume();
                timerTaskHour.resume();
            }
        });


        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (timerTaskSec != null) {
                    timerTaskSec.cancel(true);
                }

                timerTaskSec = new TimerTask(new TimerTask.TimerTaskEvents() {
                    @Override
                    public void timechanged(Integer time) {
                        TextView txtTimerSec = findViewById(R.id.txtTimerSec);
                        txtTimerSec.setText(String.format("%02d", time));
                    }
                });
                timerTaskSec.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, 1, 60);
                if (timerTaskMin != null) {
                    timerTaskMin.cancel(true);
                }
                timerTaskMin = new TimerTask(new TimerTask.TimerTaskEvents() {
                    @Override
                    public void timechanged(Integer time) {

                        TextView txtTimerMin = findViewById(R.id.txtTimerMin);
                        txtTimerMin.setText(String.format("%02d:", time));
                    }
                });
                timerTaskMin.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, 60, 60);
                if (timerTaskHour != null) {
                    timerTaskHour.cancel(true);
                }
                timerTaskHour = new TimerTask(new TimerTask.TimerTaskEvents() {
                    @Override
                    public void timechanged(Integer time) {
                        TextView txtTimerHour = findViewById(R.id.txtTimerHour);


                        txtTimerHour.setText(String.format("%02d:", time));
                    }
                });
                timerTaskHour.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, 3600, 24);
            }
        });
        btnPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timerTaskSec.pause();
                timerTaskMin.pause();
                timerTaskHour.pause();

            }
        });
    }


    @Override
    public void timechanged(Integer time) {

    }
}
