package com.example.learningandroid;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class UserCreateActivity extends Activity implements View.OnClickListener {
    EditText edName;
    EditText edPhone;
    EditText edProfit;
    TextView txtDepartment;
    Button okBtn;
    int departmentInt;
    final int departmentId0 = 0;
    final int departmentId1 = 1;
    final int departmentId2 = 2;
    final int departmentId3 = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_create_activity);
        okBtn = findViewById(R.id.okBtn);
        edName = findViewById(R.id.edName);
        edPhone = findViewById(R.id.edPhone);
        edProfit = findViewById(R.id.edProfit);
        txtDepartment = findViewById(R.id.txtDepartment);
        okBtn.setOnClickListener(this);
        registerForContextMenu(txtDepartment);

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        // TODO Auto-generated method stub
        menu.add(0, departmentId0, 0, "Производственный отдел");
        menu.add(0, departmentId1, 0, "Бухгалтерия");
        menu.add(0, departmentId2, 0, "Проектный отдел");
        menu.add(0, departmentId3, 0, "Приемная");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {

            case departmentId0:
                txtDepartment.setText("Производственный отдел");
                departmentInt=0;
                break;
            case departmentId1:
                txtDepartment.setText("Бухгалтерия");
                departmentInt=1;
                break;
            case departmentId2:
                txtDepartment.setText("Проектный отдел");
                departmentInt=2;
                break;
            case departmentId3:
                txtDepartment.setText("Приемная");
                departmentInt=3;
                break;

        }
        return super.onContextItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        Intent intent = new Intent();
        intent.putExtra("name", edName.getText().toString());
        intent.putExtra("phone", edPhone.getText().toString());
        intent.putExtra("profit", edProfit.getText().toString());
        intent.putExtra("department", departmentInt);
        setResult(RESULT_OK, intent);
        finish();
    }

}
