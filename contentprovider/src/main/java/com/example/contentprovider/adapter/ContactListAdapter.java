package com.example.contentprovider.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.contentprovider.MainActivity;
import com.example.contentprovider.R;
import com.example.contentprovider.models.PhoneEntity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ContactListAdapter extends BaseAdapter {
    List<PhoneEntity> phones;


    public void sortById() {
        Collections.sort(phones, new Comparator<PhoneEntity>() {
            @Override
            public int compare(PhoneEntity thisPhone, PhoneEntity otherPhone) {
                return thisPhone.getId().compareTo(otherPhone.getId());
            }
        });
        notifyDataSetChanged();
    }

    public void sortByName() {
        Collections.sort(phones, new Comparator<PhoneEntity>() {
            @Override
            public int compare(PhoneEntity thisPhone, PhoneEntity otherPhone) {
                return thisPhone.getName().compareTo(otherPhone.getName());
            }
        });
        notifyDataSetChanged();
    }

    public ContactListAdapter(List<PhoneEntity> phones) {
        this.phones = phones;

    }

    @Override
    public int getCount() {
        return phones.size();
    }

    @Override
    public PhoneEntity getItem(int position) {

        return phones.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_contacts, null);
        }
        PhoneEntity phone = getItem(position);
        TextView txtId = convertView.findViewById(R.id.id);
        TextView txtName = convertView.findViewById(R.id.name);


        txtId.setText(phone.getId());
        txtName.setText(phone.getName());
        return convertView;
    }
}
