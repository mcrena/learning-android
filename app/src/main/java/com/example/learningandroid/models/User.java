package com.example.learningandroid.models;

/**
 * Created by Dashkevich Makar on 01/04/2020.
 * mcrena@mail.ru
 */
public class User implements Comparable<User> {

    private String name;
    private String phone;
    private int departmentId;
    private int profit;


    public User(String name, String phone, int departmentId,int profit) {
        this.name = name;
        this.phone = phone;
        this.departmentId = departmentId;
        this.profit=profit;

    }

    public int getProfit() { return profit; }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    @Override
    public int compareTo(User user) {
        return Integer.compare(this.profit,user.getProfit());
    }

}
