package com.example.asynctasks;

import android.annotation.SuppressLint;
import android.os.AsyncTask;


class TimerTask extends AsyncTask<Integer, Integer, Void > {
    public interface TimerTaskEvents{
        void timechanged(Integer time);
    }
    TimerTaskEvents listner;

    public TimerTask(TimerTaskEvents listner) {

        this.listner = listner;
    }

    Boolean pause = false;


    @Override
    protected void onProgressUpdate(Integer... values) {

        super.onProgressUpdate(values);
        listner.timechanged(values[0]);


    }

    protected void resume() {
        pause = false;

    }

    protected void pause() {
        pause = true;
    }

    @SuppressLint("WrongThread")
    @Override
    protected Void doInBackground(Integer[] values) {
        int count = 0;
        int sleep=values[0];
                int limit=values[1];
        while (!isCancelled()) {
            try {
                Thread.sleep(sleep*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (!pause) {

                count++;
                if(count%limit==0){
                    count=0;
                }
                publishProgress(count);

            }

        }
            return null;
        }


}
