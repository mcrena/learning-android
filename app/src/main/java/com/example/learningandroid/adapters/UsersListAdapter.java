package com.example.learningandroid.adapters;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.learningandroid.R;
import com.example.learningandroid.models.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Dashkevich Makar on 01/04/2020.
 * mcrena@mail.ru
 */

public class UsersListAdapter extends BaseAdapter {
    public interface UsersListAdapterEvents {
        void onUserClick(User user);

        void onDepartamentClick(String departamentName);
    }

    private List<User> users;
    private UsersListAdapterEvents listner;
    private Map<Integer, String> departmentNames;
    private List<UserListWrapper> data;





    class UserListWrapper {
        String departmentName;
        User user;
        Integer departmentId;
        Integer positionUser;

        public UserListWrapper(String departmentName, User user, Integer departmentId,Integer positionUser) {
            this.departmentName = departmentName;
            this.user = user;
            this.departmentId = departmentId;
            this.positionUser=positionUser;

        }
    }
    public void addUser(User newUser){
        if (newUser!=null){
            users.add(newUser);
            init();
            notifyDataSetChanged();
                   }
    }
    private void init(){
Integer positionUser=0;
        data = new ArrayList<>();


        Set<Integer> departmentIds = new HashSet();
            for (User user : users) {
            departmentIds.add(user.getDepartmentId());
        }

        for (Integer id : departmentIds) {
           List<User> departamentUsers = new ArrayList<>();

            data.add(new UserListWrapper(departmentNames.get(id), null, id,null));
            for (User user : users) {
                if (user.getDepartmentId() == id) {
                    departamentUsers.add(user);

                }

            }
            Collections.sort(departamentUsers);
            for (int i=0;i<departamentUsers.size();i++) {
User user =departamentUsers.get(i);
                data.add(new UserListWrapper(null, user, null,i%2));


                        }

        }




    }

    public UsersListAdapter(List<User> users, Map<Integer, String> departmentNames, UsersListAdapterEvents listner) {
        this.departmentNames = departmentNames;
        this.listner = listner;
        this.users=users;


                       init();
//        List<UserListWrapper> usersPO = new ArrayList<>();
//        List<UserListWrapper> userBuch = new ArrayList<>();



//        // data.add(new UserListWrapper("Производственный отдел", null));
//        data.addAll(usersPO);
//        //   data.add(new UserListWrapper("Бухгалетрия", null));
//        data.addAll(userBuch);

    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public UserListWrapper getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(final int position, View view, final ViewGroup viewGroup) {
        final UserListWrapper wrapper = getItem(position);

        if (getItemViewType(position) == 0) {
            if (view == null) {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.department_listitem, null);
            }
            TextView txt = (TextView) view;
            final String departament = wrapper.departmentName;
            txt.setText(wrapper.departmentName);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listner.onDepartamentClick(departament);
                }
            });
        } else {
            if (view == null) {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.user_listitem, null);
            }
view.setBackgroundColor(wrapper.positionUser==0 ? Color.BLUE:Color.YELLOW);

            final TextView txtName = view.findViewById(R.id.txtName);
            TextView txtPhone = view.findViewById(R.id.txtPhone);
            final TextView txtProfit = view.findViewById(R.id.profit);
            TextView txtDepartament = view.findViewById(R.id.departamentName);
            final User user = wrapper.user;
            Button btnDelete = view.findViewById(R.id.btnDelete);








            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    users.remove(user);
                    init();
                    notifyDataSetChanged();



                }
            });

            txtName.setText(user.getName());
            txtPhone.setText(user.getPhone());
            String departamentName = departmentNames.get(user.getDepartmentId());
            txtDepartament.setText(departamentName);
            txtProfit.setText(String.valueOf(user.getProfit()));
            txtProfit.setTextColor(user.getProfit() <= 30000 ? Color.GREEN : Color.RED);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listner.onUserClick(user);
                }
            });
        }



        return view;
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position).departmentName != null) {
            return 0;
        }
        return 1;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

}
