package com.example.learningandroid;

import android.content.Intent;
import android.os.Bundle;

import com.example.learningandroid.adapters.UsersListAdapter;
import com.example.learningandroid.models.User;

import androidx.appcompat.app.AppCompatActivity;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements UsersListAdapter.UsersListAdapterEvents, View.OnClickListener {

    private ListView lv;
    private Button createBtn;

    User newUser;
    UsersListAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lv = findViewById(R.id.lv);
        createBtn = findViewById(R.id.createBtn);
//        ArrayAdapter<String> adapter = new ArrayAdapter(this, R.layout.user_listitem, names);
//        lv.setAdapter(adapter);
        List<User> users = new ArrayList<>();
        users.add(new User("Andrey", "+79998888888", 0, 20000));
        users.add(new User("Ivan", "+79998888877", 0, 25000));
        users.add(new User("Arina", "+79998888844", 1, 31000));
        users.add(new User("Egor", "+79998888866", 2, 500000));
        users.add(new User("Anna", "+79998888855", 3, 29000));
        adapter = new UsersListAdapter(users, getDepartmentNames(), this);
        lv.setAdapter(adapter);
        createBtn.setOnClickListener(this);

    }

    private Map<Integer, String> getDepartmentNames() {
        Map<Integer, String> departmentNames = new HashMap<>();
        departmentNames.put(0, "Производственный отдел");
        departmentNames.put(1, "Бухгалтерия");
        departmentNames.put(2, "Проектный отдел");
        departmentNames.put(3, "Приемная");
        return departmentNames;
    }


//    private void learnMap() {
//        Map<String, String> map = new HashMap<>();
//        map.put("apple", "яблоко");
//        map.put("car", "машина");
//        map.put("apple1", "яблоко");
//        Set<String> set = new HashSet();
//        set.add("car");
//        set.add("apple");
//        set.add("apple");
//        set = map.keySet();
//        Collection<String> values = map.values();
//
//        Map<User, String> users = new HashMap<>();
//        User user = new User("Ivan");
//        User user1 = new User("Andrey");
//        users.put(user, "10009");
//        users.put(user1, "10009");
//    }

//    private SharedPreferences preferences = getSharedPreferences("main", MODE_PRIVATE);
//
//    private void storeString(String key, String value) {
//        preferences.edit().putString(key, value).commit();
//        preferences.getString(key, "");
//    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onUserClick(User user) {
        Toast.makeText(this, user.getName(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDepartamentClick(String departamentName) {
        Toast.makeText(this, departamentName, Toast.LENGTH_LONG).show();
    }


    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, UserCreateActivity.class);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            return;
        }
        String name = data.getStringExtra("name");
        String phone = data.getStringExtra("phone");
        int departmentId = data.getIntExtra("department", -1);
        String profit = data.getStringExtra("profit");
        int profitInt = Integer.parseInt(profit);
        newUser=new User(name,phone,departmentId,profitInt);
        adapter.addUser(newUser);
            }

}
